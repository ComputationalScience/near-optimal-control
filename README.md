# Near-optimal control

This project provides several example implementations of neural ODE controllers. The main goal of all control examples in this repository is to steer a dynamical system from a certain initial state $`\mathbf{x}_0`$ to a desired target state $`\mathbf{x}^*`$ in finite time $`T`$. 

## Method overview

<div align="center">
<img width="600" src="drawing.svg" alt="neural ODE controller">
</div>

Panel (a) in the above figure shows a neural ODE controller that takes the time $`t`$ as an input variable and produces a control signal $`\hat{\mathbf{u}}(t;\boldsymbol{\theta})\in\mathbb{R}^m`$. A networked dynamical system is then controlled by connecting control inputs $`u_1(t;\boldsymbol{\theta}),\dots,u_m(t;\boldsymbol{\theta})`$ to all or a subset of nodes. Activation functions in the neural ODE controller are denoted by $`\sigma`$. Panels (c,d) show the evolution of $`x(t)`$, the state of a one-dimensional dynamical system, and of $`\hat{u}(t;\boldsymbol{\theta})`$, the corresponding control function. Solid grey lines represent $`x(t)`$ and $`\hat{u}(t;\boldsymbol{\theta})`$ at different training epochs. Solutions after convergence are shown by solid black lines.

## Reference
* L. Böttcher, T. Asikis, [Near-optimal control of dynamical systems with neural ordinary differential equations](https://iopscience.iop.org/article/10.1088/2632-2153/ac92c3), Machine Learning: Science and Technology 3, 045004 (2022)

Please cite our work if you find the above implementations helpful for your own research projects.

```
@article{bottcher2022near,
  title={Near-optimal control of dynamical systems with neural ordinary differential equations},
  author={B{\"o}ttcher, Lucas and Asikis, Thomas},
  journal={Machine Learning: Science and Technology},
  volume={3},
  pages={045004},
  year={2022}
}
```
